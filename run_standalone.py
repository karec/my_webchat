from __future__ import print_function
from gevent.wsgi import WSGIServer
from application import application
import argparse

parser = argparse.ArgumentParser(description='Take ip address and port number')
parser.add_argument('ip', metavar='A',  help='Ip address')
parser.add_argument('port', metavar='P', type=int, help='Port number')

args = parser.parse_args()

print('Serving on ' + args.ip + ':' + str(args.port))
WSGIServer((args.ip, args.port), application).serve_forever()
