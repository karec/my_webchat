### Goal of this project ###

This project is a little exemple and a way for me for testing gevent integration with django

### What's inside ? ###

This project is a very simple webchat with django auth and registrations features

### How to run the demo ###

Simply install the requirements in your virtualenv :


```
#!shell

pip install -r requirements.txt
```

And then run the standalone file 


```
#!shell

pyhon run_standalone.py <ip_address> <port_number>
```