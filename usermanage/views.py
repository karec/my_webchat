# -*- coding:utf-8 -*-
from django.views.generic import FormView
from usermanage.forms import UserRegistration
from django.contrib.auth.models import User
from django.contrib.messages.views import SuccessMessageMixin
from django.core.urlresolvers import reverse


class RegisterView(SuccessMessageMixin, FormView):

    template_name = "register.html"
    form_class = UserRegistration
    success_message = "Compte créé"
    success_url = '/login/'

    def form_valid(self, form):
        user = User.objects.create_user(form.cleaned_data['username'],
                                        form.cleaned_data['email'],
                                        form.cleaned_data['password'])
        user.save()
        return super(RegisterView, self).form_valid(form)
