# -*- coding:utf-8 -*-
from django.forms import ModelForm
from django import forms
from django.contrib.auth.models import User


class UserRegistration(ModelForm):

    email = forms.CharField(max_length=75, required=True, widget=forms.EmailInput)
    password = forms.CharField(max_length=40, required=True, widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = ['username', 'password', 'email']

    def clean_email(self):
        data = self.cleaned_data['email']
        if User.objects.filter(email=data).exists():
            raise forms.ValidationError("Adresse mail déjà utilisée")
        return data
