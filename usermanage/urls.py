from django.conf.urls import patterns, include, url
from views import RegisterView

urlpatterns = patterns(
    '',
    url(r'^$', RegisterView.as_view()),
    url(r'^register/$', RegisterView.as_view(), name='register'),
)
