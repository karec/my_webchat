# -*- coding:utf-8 -*-
from django.db import models
from django.contrib.auth.models import User
from django.dispatch import receiver
from django.db.models.signals import pre_save
from django.core.exceptions import ValidationError


# Va nous permettre d'intercepter les inscription
# pour forcer l'adresse mail unique pour chaque user
@receiver(pre_save, sender=User)
def User_pre_save(sender, **kwargs):
    email = kwargs['instance'].email
    username = kwargs['instance'].username

    if not email:
        raise ValidationError("Addresse email obligatoire")
    if sender.objects.filter(email=email).exclude(username=username).count():
        raise ValidationError("Addresse email déjà utilisée")


class Profil(models.Model):

    user = models.OneToOneField(User, verbose_name='utilisateur')
    image = models.ImageField(upload_to='profils')
    pole = models.CharField(max_length=100)

    def __str__(self):
        return self.user.username
