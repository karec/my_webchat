from django.conf.urls import patterns, include, url
from tracker.views import Home
from bugtracker import settings
from django.conf.urls.static import static

urlpatterns = patterns(
    '',
    url(r'^$', Home.as_view(), name='home'),
    url(r'^a/message/new$', Home.message_new),
    url(r'^a/message/updates$', Home.message_updates),
)
