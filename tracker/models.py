# -*- coding:utf-8 -*-
from django.db import models
from django.contrib.auth.models import User


class Project(models.Model):
    """
    Model de gestion des projets
    """
    name = models.CharField("Nom", max_length=100)


class ProjectMember(models.Model):
    """
    Model de gestion des membres d'un projet
    """
    user = models.ForeignKey(User, verbose_name="Utilisateur")
    project = models.ForeignKey(Project, verbose_name="Projet")


class Bug(models.Model):
    """
    Model de gestion des tickets
    TODO ajouter les choix
    """
    project = models.ForeignKey(Project, verbose_name="Projet")
    title = models.CharField("Titre", max_length=255)
    description = models.TextField("Description")
    bugType = models.CharField("Type", max_length=2)
    priority = models.CharField("Priorité", max_length=2)
    status = models.CharField("Statut", max_length=2)
    assigned = models.ForeignKey(User, verbose_name="Assigné à")
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)


class Comment(models.Model):
    """
    Model de gestion des commentaires sur les bugs
    """
    bug = models.ForeignKey(Bug, verbose_name="Bug")
    user = models.ForeignKey(User, verbose_name="Utilisateur")
    content = models.TextField("Contenu")
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
