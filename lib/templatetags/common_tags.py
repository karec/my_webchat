from django import template

register = template.Library()


@register.filter(name='addcss')
def addcss(value, arg):
    return value.as_widget(attrs={'class': arg})


@register.filter(name='addph')
def addph(value, arg):
    return value.as_widget(attrs={'placeholder': arg})


@register.filter(name='addattr')
def addattr(value, arg):
    attrs = value.field.widget.attrs

    data = arg.replace(' ', '')

    kvs = data.split(',')

    for string in kvs:
        kv = string.split(':')
        attrs[kv[0]] = kv[1]

    rendered = str(value)

    return rendered
