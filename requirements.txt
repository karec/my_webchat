Django==1.6.1
Pillow==2.2.2
argparse==1.2.1
django-grappelli==2.5.1
gevent==1.0
greenlet==0.4.1
simplejson==3.3.2
wsgiref==0.1.2
