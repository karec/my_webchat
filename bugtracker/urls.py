from django.conf.urls import patterns, include, url
from bugtracker import settings
from django.conf.urls.static import static

from django.contrib import admin
admin.autodiscover()


urlpatterns = patterns(
    '',
    url(r'^tracker/', include('tracker.urls')),
    url(r'^user/', include('usermanage.urls')),
    url(r'^panel/', include(admin.site.urls)),
    url(r'^login/$', 'django.contrib.auth.views.login', name="login"),
    url(r'^logout/$', 'django.contrib.auth.views.logout_then_login'),
) + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
